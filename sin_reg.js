function sin_reg(){};

sin_reg.prototype.getMLCoefficients = function(points_arr){
	//helper functions
	function test_function(x, a1, a0, b1, b0){
		return Math.sin(a1*x-a0)+Math.sin(b1*x-b0)
	}
	function least_square_error(points_arr, a1, a0, b1, b0){
		var S = 0;
		points_arr.map(function(point, point_index){
			var x = point.x;
			var y = point.y;
			var hidden_y = test_function(x, a1, a0, b1, b0);

			S += Math.pow(y-test_function(hidden_y, a1, a0, b1, b0), 2);
		});
		return S;	
	}
	function highest_delta(a, b){
		if(a.delta_S < b.delta_S){
			return 1
		}
		if(a.delta_S > b.delta_S){
			return -1
		}
		return 0
	}
	function lowest_S(a, b){
		if(a.S < b.S){
			return -1
		}
		if(a.S > b.S){
			return 1
		}
		return 0
	}
	//start here
	var a1 = null;
	var a0 = null;
	var b1 = null;
	var b0 = null;
	var S = Number.POSITIVE_INFINITY;
	var delta_S = Number.NEGATIVE_INFINITY;
	//NEED TO FIGURE OUT A WAY TO MAKE THIS CONVERGE RATHER THAN LOOPING THROUGH EVERY NUMBER
	var  big_o = 0;
	/*var j2=0; //0.4
	var i2=9; //9.4
	var j=3;   //3.7
	var i=6;  //6.3*/
	var tested_coef = [];
for (var j2 = 0; j2 < 5; j2=j2+.5) {
	for (var i2 = 1; i2 < 12; i2=i2+.5) {
		for (var j = 0; j < 5; j=j+.5) {
			for (var i = 1; i < 12; i=i+.5) {
					var count = 0;
					var test_arr = [];
					var x1 = i;
					var x2 = j;
					var x3 = i2;
					var x4 = j2;
					
					if(x3>x1 && x4> x2){
						//based on the symmetry of the problem
						break;
					} /*else {
						//tested_coef.push([x1, x2, x3, x4]);
					}*/
					var test0 = least_square_error(points_arr, x1, x2, x3, x4);
					//console.log('test0: ', test0);
					
					
					
					while(count<20){
						//console.log('\n\n NEW COUNT: ', count);
						big_o++;
						var test1A = least_square_error(points_arr, x1+0.1, x2, x3, x4);
						//console.log('test1A: ', test1A);
						test_arr.push({delta_S: test0-test1A, S: test1A, a1: x1+.01, a0: x2, b1: x3, b0: x4});
						
						var test2A = least_square_error(points_arr, x1, x2+0.1, x3, x4);
						//console.log('test2A: ', test2A);
						test_arr.push({delta_S: test0-test2A, S: test2A, a1: x1, a0: x2+.01, b1: x3, b0: x4});
						
						var test3A = least_square_error(points_arr, x1, x2, x3+0.1, x4);
						//console.log('test3A: ', test3A);
						test_arr.push({delta_S: test0-test3A, S: test3A, a1: x1, a0: x2, b1: x3+.01, b0: x4});
						
						var test4A = least_square_error(points_arr, x1, x2, x3, x4+0.1);
						//console.log('test4A: ', test4A);
						test_arr.push({delta_S: test0-test4A, S: test4A, a1: x1, a0: x2, b1: x3, b0: x4+.01});
						
		
						if(test1A<=test0 && (test0-test1A)>0.1){
						 x1=x1+0.1;
						 //console.log('x1 went up by .1 to: ', x1)
						} else if(test1A>=test0 && (test1A-test0)>0.1  && x1>.1){
						 x1=x1-0.1;
						 //console.log('x1 went down by .1 to: ', x1)
						}

						if(test2A<=test0&& (test0-test2A)>0.1){
						 x2=x2+0.1;
						 //console.log('x2 went up by .1 to: ', x2)
						} else if(test2A>=test0&& (test2A-test0)>0.1 && x2>.1){
						 x2=x2-0.1;
						 //console.log('x2 went down by .1 to: ', x2)
						}

						if(test3A<=test0&& (test0-test3A)>0.1){
						 x3=x3+0.1;
						 //console.log('x3 went up by .1 to: ', x3)
						} else if(test3A>=test0&& (test3A-test0)>0.1 && x3>.1){
						 x3=x3-0.1;
						 //console.log('x3 went down by .1 to: ', x3)
						}

						if(test4A<=test0&& (test0-test4A)>0.1){
						 x4=x4+0.1;
						 //console.log('x4 went up by .1 to: ', x4)
						} else if(test4A>=test0&& (test4A-test0)>0.1 && x4>.1){
						 x4=x4-0.1;
						 //console.log('x4 went down by .1 to: ', x4)
						}
						var test5A = least_square_error(points_arr, x1, x2, x3, x4);
						test_arr.push({delta_S: test0-test5A, S: test5A, a1: x1, a0: x2, b1: x3, b0: x4});
						//console.log('Test0: ', test0);
						//console.log('Test1A: ', test1A);
						//console.log('Test5A:', test5A);
						test_arr.sort(lowest_S);
						//console.log("SORTED?", test_arr);
						if(test_arr[0].S < test0){
							//console.log("it worked on count: ", count);
							test0 = test_arr[0].S;
							test_arr = [test_arr[0]];
						} else {
							//console.log('it converged on count: ', count);
							if(test_arr[0].S<S){
								S=test_arr[0].S;
								a1 = test_arr[0].a1;
								a0 = test_arr[0].a0;
								b1 = test_arr[0].b1;
								b0 = test_arr[0].b0;	
							}
							break;
						}
						count++;
						
					}
				}
			}
		}
	}
	console.log("big_o: ", big_o);
	//console.log("tested_coef's: ", tested_coef.length);
	return {a1: a1, a0: a0, b1: b1, b0: b0, S: S};
}
sin_reg.prototype.printSinWeek = function(output_obj, data){
	//helper functions
	function test_function(x, a1, a0, b1, b0){
		return Math.sin(a1*x-a0)+Math.sin(b1*x-b0)
	}
	function getMLFunc(radians, radius, a1, a0, b1, b0) {
	  return radius*(test_function(test_function(radians, a1, a0, b1, b0), a1, a0, b1, b0))
	}
	function activationFunc(theta){
		if(Math.abs(theta)>0.3) return 1;
		return 0;
	}
	//start here
	var output_data = [];
	data.map((data_item, data_index)=>{
		console.log(getMLFunc(data_item, 1, output_obj.a1, output_obj.a0, output_obj.b1, output_obj.b0));
		output_data.push(activationFunc(getMLFunc(data_item, 1, output_obj.a1, output_obj.a0, output_obj.b1, output_obj.b0)));
	});
	return output_data;
}

if (typeof module !== 'undefined' && typeof module.exports !== 'undefined'){
	module.exports = sin_reg;
} else {
	window.sin_reg = sin_reg;
}