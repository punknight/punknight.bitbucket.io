function Gantt(arr){
	this.arr = arr;
}

Gantt.prototype.validate = function(){
	var gantt_arr = this.arr.map((gantt_obj)=>{
		if( !gantt_obj.hasOwnProperty('gantt_id')) gantt_obj.gantt_id = generateID('gantt');
		if( !gantt_obj.hasOwnProperty('goal_id')) throw 'gantt_obj needs a valid goal_id';
		if( !gantt_obj.hasOwnProperty('numerator_hours')) gantt_obj.numerator_hours = 1;
		if( !gantt_obj.hasOwnProperty('denominator_hours')) gantt_obj.denominator_hours = 1;
		if( !gantt_obj.hasOwnProperty('calendar_id')) throw 'gantt_obj needs a valid calendar_id';
		if( !gantt_obj.hasOwnProperty('status')) gantt_obj.status= 'IN_PROGRESS';
		if( !gantt_obj.hasOwnProperty('color')) gantt_obj.color = '#66ADD5';
		if( !gantt_obj.hasOwnProperty('journal_text')) gantt_obj.journal_text = '';
		return gantt_obj;
	});
	this.arr = gantt_arr;
}

Gantt.prototype.addFromTask = function(task_obj){
	var gantt_arr = this.arr;
	var gantt_index = gantt_arr.findIndex((item)=>{
		return (item.calendar_id==task_obj.calendar_id && task_obj.goal_id==item.goal_id)
	});
	if(gantt_index ==-1){
		var gantt_id = generateID('gantt');
		var new_gantt_obj = {
			id: gantt_id,
			goal_id: task_obj.goal_id,
			calendar_id: task_obj.calendar_id,
			numerator_hours: task_obj.numerator_hours,
			denominator_hours: task_obj.denominator_hours,
			status: 'PROJECTED',
			color: task_obj.color
		};
		gantt_arr.push(new_gantt_obj);
	} else {
		var old_gantt_obj = gantt_arr[gantt_index];
		if(old_gantt_obj.status === 'PROJECTED'){
			old_gantt_obj.status = 'IN_PROGRESS';	
			old_gantt_obj.numerator_hours = task_obj.numerator_hours;
		} else {
			old_gantt_obj.numerator_hours = old_gantt_obj.numerator_hours+task_obj.numerator_hours;	
		}
		gantt_arr[gantt_index] = old_gantt_obj;
	}
	this.arr = gantt_arr;
	return this;
}

Gantt.prototype.addFromPolicy = function(policy_obj, single_index_input, new_date_calc){
	console.log('ADD FROM POLICY policy_obj', policy_obj);
	console.log('ADD FROM POLICY single_index_input', single_index_input);
	console.log('new_date_calc: ', new_date_calc);
	var gantt_arr = this.arr;
	var calendar_id = new_date_calc.date_adder([0, 0, single_index_input]).toData();
	console.log("calendar_id: ", calendar_id);
	var gantt_index = gantt_arr.findIndex((item)=>{
		return (item.calendar_id==calendar_id && policy_obj.goal_id==item.goal_id)
	});
	if(gantt_index ==-1){
		var gantt_id = generateID('gantt');
		var new_gantt_obj = {
			id: gantt_id,
			goal_id: policy_obj.goal_id,
			calendar_id: calendar_id,
			numerator_hours: policy_obj.denominator_hours,
			denominator_hours: policy_obj.denominator_hours,
			status: 'PROJECTED',
			color: policy_obj.color
		};
		gantt_arr.push(new_gantt_obj);
		
	} else {
		//NOTHING
	}
	this.arr = gantt_arr;
	return this;
}

Gantt.prototype.addSingle = function(policy_obj, date_calc){
	var gantt_arr = this.arr;
	var calendar_id = date_calc.toData();
	var gantt_index = gantt_arr.findIndex((item)=>{
		return (item.calendar_id==calendar_id && policy_obj.goal_id==item.goal_id)
	});
	if(gantt_index ==-1){
		var gantt_id = generateID('gantt');
		var new_gantt_obj = {
			id: gantt_id,
			goal_id: policy_obj.goal_id,
			calendar_id: calendar_id,
			numerator_hours: policy_obj.denominator_hours,
			denominator_hours: policy_obj.denominator_hours,
			status: 'PROJECTED',
			color: policy_obj.color
		};
		gantt_arr.push(new_gantt_obj);
		
	} else {
		//it already exists
	}
	this.arr = gantt_arr;
}

Gantt.prototype.addFromML = function(output_arr, initial_view_date, policy_obj){
	var gantt_arr = this.arr;
	

	output_arr.map((item, index)=>{
		var calendar_id = initial_view_date.date_adder([0, 0, index]).toData();
		var found_index = gantt_arr.findIndex((gantt_item)=>{
			return (gantt_item.goal_id==policy_obj.goal_id &&calendar_id==gantt_item.calendar_id);
		});
		if(item==1 && found_index==-1){
			var gantt_id = generateID('gantt');
			var new_gantt_obj = {
				id: gantt_id,
				goal_id: policy_obj.goal_id,
				calendar_id: calendar_id,
				numerator_hours: policy_obj.denominator_hours,
				denominator_hours: policy_obj.denominator_hours,
				status: 'PROJECTED',
				color: policy_obj.color
			};
			gantt_arr.push(new_gantt_obj);
		}
	})
	this.arr = gantt_arr;
	return this;
}

Gantt.prototype.toString = function(){
	return this.arr;
}

Gantt.prototype.removeFromTask = function(task_obj){
	var gantt_arr = this.arr;

	var gantt_index = gantt_arr.findIndex((item)=>{
		return (item.calendar_id==task_obj.calendar_id && task_obj.goal_id==item.goal_id)
	});
	if(gantt_index ==-1){
		throw "error will robinson!";
	} else {
		var old_gantt_obj = gantt_arr[gantt_index];
		old_gantt_obj.numerator_hours = old_gantt_obj.numerator_hours-task_obj.numerator_hours;
		if(old_gantt_obj.numerator_hours<=0){
			old_gantt_obj.status = 'PROJECTED';
			old_gantt_obj.numerator_hours = old_gantt_obj.denominator_hours;
		} 
		gantt_arr[gantt_index] = old_gantt_obj;	
		
	}	
	this.arr = gantt_arr;
	return this;
}
Gantt.prototype.removeFromPolicy = function(policy_obj){
	var gantt_arr = this.arr;
	var new_gantt_arr = gantt_arr.filter((item)=>{
		return (item.status=='IN_PROGRESS' || !(policy_obj.goal_id==item.goal_id))
	});
	this.arr = new_gantt_arr;
	return this;
}


function generateID(section_name='O'){
	var id = section_name.substr(0, 1)+"_"+Math.random().toString(36).substr(2, 9);
	return id;
}

function getColor(){
	color_arr = [
		'#66ADD5',
		'#D56666',
		'#66D58F'
	]
	return color_arr[Math.floor(Math.random()*color_arr.length)];
}

if (typeof module !== 'undefined' && typeof module.exports !== 'undefined'){
	module.exports = Gantt;
} else {
	window.Gantt = Gantt;
}