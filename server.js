var app = require('express')();
var serveStatic = require('serve-static');
var http = require('http').Server(app);
var io = require('socket.io')(http);


app.use(serveStatic(__dirname + '/'))

app.get('/', function(req, res) {
	var test = Math.random();
	if(test<.8){
		res.sendFile(__dirname+'/index.html');	
	} else {
		res.sendFile(__dirname+'/hypothesis.html');
	}
});

http.listen(process.env.PORT || 3000, function(){
	console.log('server is up and running on port 3000');
});

io.on('connection', function(socket){
	console.log('new client connected on socket: ', socket.id);
	socket.on('LOGIN', function(obj){
		console.log('LOGIN FIRED', obj);
		io.to(socket.id).emit('confirm_args', {
			err: null, 
			msg: {type:'msg_success', text: 'logged in.'}});	
	});
});