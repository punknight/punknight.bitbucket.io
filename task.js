function Task(arr){
	this.arr = arr;
}


function generateID(section_name='O'){
	var id = section_name.substr(0, 1)+"_"+Math.random().toString(36).substr(2, 9);
	return id;
}

Task.prototype.validate = function(){
	var task_arr = this.arr.map((task_obj)=>{
		if( !task_obj.hasOwnProperty('goal_id')) throw 'task_obj needs a valid goal_id';
		if( !task_obj.hasOwnProperty('numerator_hours')) task_obj.numerator_hours = 1;
		if( !task_obj.hasOwnProperty('denominator_hours')) task_obj.denominator_hours = 1;
		if( !task_obj.hasOwnProperty('numerator_num')) task_obj.numerator_num = 1;
		if( !task_obj.hasOwnProperty('calendar_id')) throw 'task_obj needs a valid calendar_id';
		if( !task_obj.hasOwnProperty('status')) task_obj.status= 'INCOMPLETE';
		if( !task_obj.hasOwnProperty('description')) task_obj.description = 'part 1, feel free to edit';
		if( !task_obj.hasOwnProperty('color')) task_obj.color = '#66ADD5';
		if( !task_obj.hasOwnProperty('time')) task_obj.time = '06:00';
		return task_obj;
	});
	
	this.arr = task_arr;
}

Task.prototype.removeFromPolicy = function(policy_obj){
	var task_arr = this.arr;
	var remove_arr = [];
	task_arr.map((item, index)=>{
		if(item.status=='INCOMPLETE' && policy_obj.goal_id==item.goal_id){
			remove_arr.push(index)
		}
	});
	for (var i = remove_arr.length - 1; i >= 0; i--) {
		task_arr.splice(remove_arr[i], 1);
	};
	this.arr = task_arr;
	return this;
}

Task.prototype.addFromPolicy = function(policy_obj, single_index_input, date_calc_today){
	//three states: 
		//1) no task with same goal_id & calendar_id; 
		//2) task with same goal_id & calendar_id, but numerator is not filled
		//3) task with same goal_id & calendar_id, and numerator is filled
	var task_arr = this.arr;
	var calendar_id = date_calc_today.date_adder([0, 0, single_index_input]).toData();
	
	var task_filter_arr = task_arr.filter((item)=>{
		return (item.calendar_id==calendar_id && policy_obj.goal_id==item.goal_id)
	});


	if(task_filter_arr.length === 0){
		//console.log('im here1: calendar_id: ', calendar_id);
		var new_task_arr = [];
		for (var i = 0; i < policy_obj.denominator_hours/policy_obj.numerator_hours; i++) {
			var task_id = generateID('task');
			var description = policy_obj.description.replace("#", i+1);
			var time_number = timeToNumber(policy_obj.time)+i*100;
			var hour = Math.trunc(time_number/100);
			var min = time_number-hour*100;
			min = (min<10) ? "0"+min : min;
			var time_data = (hour<10) ? "0"+hour+":"+min : hour+":"+min;
			
			var new_task_obj = {
				task_id: task_id,
				time: time_data,
				goal_id: policy_obj.goal_id,
				calendar_id: calendar_id,
				numerator_hours: policy_obj.numerator_hours,
				denominator_hours: policy_obj.denominator_hours,
				numerator_num: i+1,
				status: 'INCOMPLETE',
				description: description,
				color: policy_obj.color
			}
			new_task_arr.push(new_task_obj);
		}
		task_arr = task_arr.concat(new_task_arr);
		this.arr = task_arr;
	} else if (task_filter_arr.length < policy_obj.denominator_hours/policy_obj.numerator_hours) {
		var new_task_arr = [];
		for (var i = task_filter_arr.length; i < policy_obj.denominator_hours/policy_obj.numerator_hours; i++) {
			var task_id = generateID('task');
			var description = policy_obj.description.replace("#", i+1);
			var time_number = timeToNumber(policy_obj.time)+i*100;
			var hour = Math.trunc(time_number/100);
			var min = time_number-hour*100;
			min = (min<10) ? "0"+min : min;
			var time_data = (hour<10) ? "0"+hour+":"+min : hour+":"+min;
			
			var new_task_obj = {
				task_id: task_id,
				time: time_data,
				goal_id: policy_obj.goal_id,
				calendar_id: calendar_id,
				numerator_hours: policy_obj.numerator_hours,
				denominator_hours: policy_obj.denominator_hours,
				numerator_num: i+1,
				status: 'INCOMPLETE',
				description: description,
				color: policy_obj.color
			}
			new_task_arr.push(new_task_obj);
		}
		task_arr = task_arr.concat(new_task_arr);
		this.arr = task_arr;	
	} else {
			//check the next calendar date 
			//console.log('im here3');
			//do nothing because higher layer algorithm will test the next case;
	}
}

Task.prototype.addSingle = function(policy_obj, date_calc){
	var task_arr = this.arr;
	var calendar_id = date_calc.toData();
	var task_filter_arr = task_arr.filter((item)=>{
		return (item.calendar_id==calendar_id && policy_obj.goal_id==item.goal_id)
	});


	if(task_filter_arr.length === 0){
		
		var new_task_arr = [];
		for (var i = 0; i < policy_obj.denominator_hours/policy_obj.numerator_hours; i++) {
			var task_id = generateID('task');
			var description = policy_obj.description.replace("#", i+1);
			var time_number = timeToNumber(policy_obj.time)+i*100;
			var hour = Math.trunc(time_number/100);
			var min = time_number-hour*100;
			min = (min<10) ? "0"+min : min;
			var time_data = (hour<10) ? "0"+hour+":"+min : hour+":"+min;
		
			var new_task_obj = {
				task_id: task_id,
				time: time_data,
				goal_id: policy_obj.goal_id,
				calendar_id: calendar_id,
				numerator_hours: policy_obj.numerator_hours,
				denominator_hours: policy_obj.denominator_hours,
				numerator_num: i+1,
				status: 'INCOMPLETE',
				description: description,
				color: policy_obj.color
			}
			new_task_arr.push(new_task_obj);
		}
		task_arr = task_arr.concat(new_task_arr);
		this.arr = task_arr;
	} else if (task_filter_arr.length < policy_obj.denominator_hours/policy_obj.numerator_hours) {
		var new_task_arr = [];
		for (var i = task_filter_arr.length; i < policy_obj.denominator_hours/policy_obj.numerator_hours; i++) {
			var task_id = generateID('task');
			var description = policy_obj.description.replace("#", i+1);
			var time_number = timeToNumber(policy_obj.time)+i*100;
			var hour = Math.trunc(time_number/100);
			var min = time_number-hour*100;
			console.log("min: ", min);
			min = (min<10) ? "0"+min : min;
			var time_data = (hour<10) ? "0"+hour+":"+min : hour+":"+min;

			var new_task_obj = {
				task_id: task_id,
				time: time_data,
				goal_id: policy_obj.goal_id,
				calendar_id: calendar_id,
				numerator_hours: policy_obj.numerator_hours,
				denominator_hours: policy_obj.denominator_hours,
				numerator_num: i+1,
				status: 'INCOMPLETE',
				description: description,
				color: policy_obj.color
			}
			new_task_arr.push(new_task_obj);
		}
		task_arr = task_arr.concat(new_task_arr);
		this.arr = task_arr;	
	} else {
		//task already exists
	}
}

Task.prototype.changeStatus = function(task_id, status){
	this.arr.forEach((item)=>{
		if(item.task_id ===task_id){
			item.status=status;
		}
	});
}
Task.prototype.changeData = function(task_id, data_obj){
	if(typeof data_obj.description == 'undefined' || typeof data_obj.time == 'undefined'){
		throw 'not enough data'
	}
	this.arr.forEach((item)=>{
		if(item.task_id ===task_id){
			item.description=description;
			item.time=time;
		}
	});
}
Task.prototype.toString = function(){
	return this.arr;
}

function timeToNumber(time){
  var hour = time.substr(0, 2);
  var min = time.substr(3, 5);
  return parseInt(hour+min);
}
function timeToString(time){
  var am_pm = null;
  var hour = time.substr(0, 2);
  var min = time.substr(3, 5);
  if(parseInt(hour)<12){
    am_pm = 'AM';
    hour = parseInt(hour);
    if(parseInt(hour)===0) hour = 12;
  } else {
    am_pm = 'PM';
    hour = parseInt(hour)-12;
    if(hour === 0) hour = 12;
  }
  return hour+':'+min+' '+am_pm;
}


if (typeof module !== 'undefined' && typeof module.exports !== 'undefined'){
	module.exports = Task;
	//dependency injection seems over convoluted at this point.
	var sin_reg_class = require('./sin_reg.js');
	var sin_reg = new sin_reg_class();
	var date_calc = require('./date_calc.js');
} else {
	window.Task = Task;
}
