function Policy(arr){
	this.arr = arr;
}

Policy.prototype.validate = function(){
	
	var policy_arr = this.arr.map((policy_obj)=>{
		if( !policy_obj.hasOwnProperty('goal_id')) throw 'policy_obj needs a valid goal_id';
		if( !policy_obj.hasOwnProperty('numerator_hours')) policy_obj.numerator_hours = 1;
		if( !policy_obj.hasOwnProperty('denominator_hours')) policy_obj.denominator_hours = 1;
		if( !policy_obj.hasOwnProperty('num_tasks')) policy_obj.num_tasks = 5;
		if( !policy_obj.hasOwnProperty('ml_coefficients')) policy_obj.ml_coefficients = { a1: 3.2, a0: 0.8, b1: 6.4, b0: 0, S: 0.27358637224958954 };
		if( !policy_obj.hasOwnProperty('word_of_affirmation')) policy_obj.word_of_affirmation = 'Good on you!';
		if( !policy_obj.hasOwnProperty('status')) policy_obj.status= 'INACTIVE';
		if( !policy_obj.hasOwnProperty('description')) policy_obj.description = 'part #, feel free to edit';
		if( !policy_obj.hasOwnProperty('color')) policy_obj.color = '#66ADD5';
		if( !policy_obj.hasOwnProperty('time')) policy_obj.time = '06:00'
		if( !policy_obj.hasOwnProperty('week')) policy_obj.week = {m: false, t:false, w:false, th:false, f:false, sa: false, su: false}
		return policy_obj;
	});
	
	this.arr = policy_arr;
}

Policy.prototype.add = function(policy_obj){
	var policy_arr = this.arr;
	var policy_index = policy_arr.findIndex((item)=>{
		return (item.goal_id==policy_obj.goal_id)
	});
	if(policy_index ==-1){
		var ml_coefficients = (policy_obj.ml_coefficients) ? policy_obj.ml_coefficients : { a1: 3.2, a0: 0.8, b1: 6.4, b0: 0, S: 0.27358637224958954 };
		var new_policy_obj = {
			goal_id: policy_obj.goal_id,
			numerator_hours: policy_obj.numerator_hours,
			denominator_hours: policy_obj.denominator_hours,
			num_tasks: policy_obj.num_tasks,
			ml_coefficients: ml_coefficients,
			week: policy_obj.week,
			initial_index_input: policy_obj.initial_index_input,
			word_of_affirmation: policy_obj.word_of_affirmation,
			status: policy_obj.status,
			time: policy_obj.time,
			description: 'part #',
			color: policy_obj.color
		};
		policy_arr.push(new_policy_obj);
		
	} else {
		throw "policy already exists";
	}
	this.arr = policy_arr;
	return this;
}

Policy.prototype.edit = function(policy_obj){
	var policy_arr = this.arr;
	var policy_index = policy_arr.findIndex((item)=>{
		return (item.goal_id==policy_obj.goal_id)
	});
	if(policy_index ==-1){
		throw "policy doesn't exists";

	} else {		
		
		var ml_coefficients = (policy_obj.ml_coefficients) ? policy_obj.ml_coefficients : { a1: 3.2, a0: 0.8, b1: 6.4, b0: 0, S: 0.27358637224958954 };
		var updated_policy_obj = {
			goal_id: policy_obj.goal_id,
			numerator_hours: policy_obj.numerator_hours,
			denominator_hours: policy_obj.denominator_hours,
			num_tasks: policy_obj.num_tasks,
			ml_coefficients: ml_coefficients,
			week: policy_obj.week,
			initial_index_input: policy_obj.initial_index_input,
			word_of_affirmation: policy_obj.word_of_affirmation,
			status: 'INACTIVE',
			time: policy_obj.time,
			description: 'part #',
			color: policy_obj.color
		};
		policy_arr[policy_index]=updated_policy_obj;
	}
	this.arr = policy_arr;
	return this;
}

Policy.prototype.destroy = function(goal_id){
	var policy_arr = this.arr;
	var policy_index = policy_arr.findIndex((item)=>{
		return (item.goal_id==goal_id)
	});
	if(policy_index ==-1){
		throw "policy doesn't exists";

	} else {		
		policy_arr.splice(policy_index, 1);
	}
	this.arr = policy_arr;
	return this;
}

Policy.prototype.changeMLcoefficients = function(goal_id, num_tasks, ml_coefficients){
	 var policy_arr = this.arr;
	 var found_index = policy_arr.findIndex((policy_obj)=>{
			return (policy_obj.goal_id==goal_id)
		});
		if(found_index!= -1){
			policy_arr[found_index].ml_coefficients = ml_coefficients;
		 policy_arr[found_index].num_tasks = num_tasks;
		}
	 
	 this.arr = policy_arr;
	 return this;
}
Policy.prototype.changeStatus = function(goal_id, status){
	
	var policy_arr = this.arr;
	var found_index = policy_arr.findIndex((policy_obj)=>{
		return (policy_obj.goal_id==goal_id)
	});
	if(found_index!= -1){
		policy_arr[found_index].status = status;
	}
	this.arr = policy_arr;
	return policy_arr[found_index];
}

Policy.prototype.getPolicyObj = function(goal_id){
	
	var policy_arr = this.arr;
	var found_index = policy_arr.findIndex((policy_obj)=>{
		return (policy_obj.goal_id==goal_id)
	});
	if(found_index == -1){
		return null
	} else {
		return policy_arr[found_index];	
	}
}

Policy.prototype.toString = function(){
	return this.arr;
}


function generateID(section_name='O'){
	var id = section_name.substr(0, 1)+"_"+Math.random().toString(36).substr(2, 9);
	return id;
}




if (typeof module !== 'undefined' && typeof module.exports !== 'undefined'){
	module.exports = Policy;
} else {
	window.Policy = Policy;
}
/*
var policy_A = {
	goal_id: 'goal-A', 
	numerator_hours: 1, 
	denominator_hours: 2, 
	num_tasks: 0,
	ml_coefficients: { a1: 3.2, a0: 0.8, b1: 6.4, b0: 0, S: 0.27358637224958954 },
	initial_index_input: 1
};
var policy_B = {
	goal_id: 'goal-B', 
	numerator_hours: 1, 
	denominator_hours: 2, 
	num_tasks: 0,
	ml_coefficients: { a1: 3.2, a0: 0.8, b1: 6.4, b0: 0, S: 0.27358637224958954 },
	initial_index_input: 1
};
var policy_C = {
	goal_id: 'goal-C', 
	numerator_hours: 1, 
	denominator_hours: 2, 
	num_tasks: 0,
	ml_coefficients: { a1: 3.2, a0: 0.8, b1: 6.4, b0: 0, S: 0.27358637224958954 },
	initial_index_input: 1
};
var policy_arr = [policy_A, policy_B, policy_C];
var policy_D = {
	goal_id: 'goal-D', 
	numerator_hours: 1, 
	denominator_hours: 3, 
	num_tasks: 0,
	ml_coefficients: { a1: 3.2, a0: 0.8, b1: 6.4, b0: 0, S: 0.27358637224958954 },
	initial_index_input: 1
};
var policy = new Policy(policy_arr);
policy.add(policy_D);
console.log(policy.toString());

*/