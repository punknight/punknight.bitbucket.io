function Data(data){
	if(typeof data == 'undefined') throw 'data param undefined';
	if(data===null){
		this.data = {gantt_arr: [], policy_arr: [], task_arr: []}	
	} else {
		this.data = data;
	}
}

Data.prototype.bootstrapJSON = function(date_calc_monday){
	var policy_arr = this.data.policy_arr.map((policy_obj)=>{
		if( !policy_obj.hasOwnProperty('goal_id') || 
				!policy_obj.hasOwnProperty('numerator_hours') ||
				!policy_obj.hasOwnProperty('denominator_hours') ||
				!policy_obj.hasOwnProperty('num_tasks') ||
				!policy_obj.hasOwnProperty('ml_coefficients') ||
				!policy_obj.hasOwnProperty('word_of_affirmation') ||
				!policy_obj.hasOwnProperty('status') ||
				!policy_obj.hasOwnProperty('description') ||
				!policy_obj.hasOwnProperty('color') ||
				!policy_obj.hasOwnProperty('time') ||
				!policy_obj.hasOwnProperty('week')
			){ 
				throw 'A policy does not have all the correct properties'
		} else {
			return policy_obj;
		}
	});

	var task_arr = this.data.task_arr.map((task_obj)=>{
		if( !task_obj.hasOwnProperty('task_id') ||
				!task_obj.hasOwnProperty('goal_id') || 
				!task_obj.hasOwnProperty('numerator_hours') ||
				!task_obj.hasOwnProperty('denominator_hours') ||
				!task_obj.hasOwnProperty('numerator_num') ||
				!task_obj.hasOwnProperty('calendar_id') ||
				!task_obj.hasOwnProperty('status') ||
				!task_obj.hasOwnProperty('description') ||
				!task_obj.hasOwnProperty('color') ||
				!task_obj.hasOwnProperty('time')
			){ 
				throw 'A policy does not have all the correct properties'
		} else {
			var calendar_id = date_calc_monday.date_adder([0, 0, task_obj.calendar_id]).toData();	
			return {
				task_id: task_obj.task_id,
				time: task_obj.time,
				goal_id: task_obj.goal_id,
				calendar_id: calendar_id,
				numerator_hours: task_obj.numerator_hours,
				denominator_hours: task_obj.denominator_hours,
				numerator_num: task_obj.numerator_num,
				status: task_obj.status,
				description: task_obj.description,
				color: task_obj.color
			}
		}
	});

	var gantt_arr = this.data.gantt_arr.map((gantt_obj)=>{
		if( !gantt_obj.hasOwnProperty('gantt_id') ||
				!gantt_obj.hasOwnProperty('goal_id') || 
				!gantt_obj.hasOwnProperty('numerator_hours') ||
				!gantt_obj.hasOwnProperty('denominator_hours') ||
				!gantt_obj.hasOwnProperty('calendar_id') ||
				!gantt_obj.hasOwnProperty('status') ||
				!gantt_obj.hasOwnProperty('color') ||
				!gantt_obj.hasOwnProperty('journal_text')
			){ 
				throw 'gantt_obj is missing an attribute';
			} else {
				var calendar_id = date_calc_monday.date_adder([0, 0, gantt_obj.calendar_id]).toData();	
				return {
					calendar_id: calendar_id,
          color: gantt_obj.color,
          denominator_hours: gantt_obj.denominator_hours,
          goal_id: gantt_obj.goal_id,
          gantt_id: gantt_obj.gantt_id,
          numerator_hours: gantt_obj.numerator_hours,
          status: gantt_obj.status,
          journal_text: gantt_obj.journal_text
        }
			}
	});	

	this.policy_arr = policy_arr;
	this.task_arr = task_arr;
	this.gantt_arr = gantt_arr;
}

if (typeof module !== 'undefined' && typeof module.exports !== 'undefined'){
	module.exports = Data;
} else {
	window.Data = Data;
}
