var assert = require('assert');
var Gantt = require('../gantt.js');
var task_arr = [
	{task_id: 't1', goal_id:'goal-A', calendar_id: '2018-6-30', numerator_hours: 1, denominator_hours: 1, status: 'INCOMPLETE'},
	{task_id: 't2', goal_id:'goal-A', calendar_id: '2018-7-1', numerator_hours: 1, denominator_hours: 1, status: 'INCOMPLETE'},
	{task_id: 't3', goal_id:'goal-A', calendar_id: '2018-7-3', numerator_hours: 1, denominator_hours: 1, status: 'INCOMPLETE'},
	{task_id: 't4', goal_id:'goal-B', calendar_id: '2018-6-30', numerator_hours: 1, denominator_hours: 2, status: 'INCOMPLETE'},
	{task_id: 't5', goal_id:'goal-B', calendar_id: '2018-6-30', numerator_hours: 1, denominator_hours: 2, status: 'INCOMPLETE'},
	{task_id: 't6', goal_id:'goal-B', calendar_id: '2018-6-31', numerator_hours: 1, denominator_hours: 2, status: 'INCOMPLETE'},
	{task_id: 't7', goal_id:'goal-B', calendar_id: '2018-6-31', numerator_hours: 1, denominator_hours: 2, status: 'INCOMPLETE'},
	{task_id: 't8', goal_id:'goal-B', calendar_id: '2018-7-2', numerator_hours: 1, denominator_hours: 2, status: 'INCOMPLETE'},
	{task_id: 't9', goal_id:'goal-B', calendar_id: '2018-7-2', numerator_hours: 1, denominator_hours: 2, status: 'INCOMPLETE'},
	{task_id: 't10', goal_id:'goal-B', calendar_id: '2018-7-3', numerator_hours: 1, denominator_hours: 2, status: 'INCOMPLETE'},
	{task_id: 't11', goal_id:'goal-B', calendar_id: '2018-7-3', numerator_hours: 1, denominator_hours: 2, status: 'INCOMPLETE'},
];
var gantt_arr = [
	{gantt_id: 'g1', goal_id: 'goal-C', calendar_id: '2018-6-30', numerator_hours: 2, denominator_hours: 3},
	{gantt_id: 'g2', goal_id: 'goal-C', calendar_id: '2018-6-31', numerator_hours: 2, denominator_hours: 3},
	{gantt_id: 'g3', goal_id: 'goal-C', calendar_id: '2018-7-1', numerator_hours: 2, denominator_hours: 3},
	{gantt_id: 'g4', goal_id: 'goal-C', calendar_id: '2018-7-4', numerator_hours: 2, denominator_hours: 3},
	{gantt_id: 'g5', goal_id: 'goal-C', calendar_id: '2018-7-5', numerator_hours: 2, denominator_hours: 3},
	{gantt_id: 'g6', goal_id: 'goal-C', calendar_id: '2018-7-6', numerator_hours: 2, denominator_hours: 3},
]
var gantt = {};
describe('Gantt', function(){
	describe('constructor', function(){
		it('should create Gantt object', function(done){
			gantt = new Gantt(gantt_arr);
			assert(gantt.arr.length==6);
			done();
		});
	});
	describe('addTask', function(){
		it('should add a bunch of tasks to the gantt instance', function(done){
			for (var i = 0; i < task_arr.length; i++) {
				gantt.addTask(task_arr[i]);
			};
			assert(gantt.arr.length==13)			
			done();
		});
	});
	describe('removeTask', function(){
		it('should remove a bunch of tasks from the gantt_instance', function(done){
			for (var i = 0; i < task_arr.length; i++) {
				gantt.removeTask(task_arr[i]);
			};
			assert(gantt.arr.length==6);
			done();
		})
	})
});






/*




console.log(gantt.toString());
*/