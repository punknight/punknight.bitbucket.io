var assert = require('assert');
var Task = require('../task.js');



var task_arr = [
	{task_id: 't1', goal_id:'goal-A', calendar_id: '2018-6-30', numerator_hours: 1, denominator_hours: 2, status: 'INCOMPLETE'},
	{task_id: 't2', goal_id:'goal-A', calendar_id: '2018-6-30', numerator_hours: 1, denominator_hours: 2, status: 'INCOMPLETE'},
	{task_id: 't3', goal_id:'goal-A', calendar_id: '2018-7-1', numerator_hours: 1, denominator_hours: 2, status: 'INCOMPLETE'},
];

var policy_obj = {
	goal_id: 'goal-A', 
	numerator_hours: 1, 
	denominator_hours: 2, 
	num_tasks: 0,
	ml_coefficients: { a1: 3.2, a0: 0.8, b1: 6.4, b0: 0, S: 0.27358637224958954 },
	inital_index_input: 1
};


var task = {};
describe('Task', function(){
	describe('constructor', function(){
		it('should create Task object', function(done){
			task = new Task(task_arr);
			assert(task.arr.length==3);
			done();
		});
	});
	describe('addFromPolicy', function(){
		it('should add a bunch of tasks to the task instance', function(done){
			task.addFromPolicy(policy_obj, '2018-6-29');
			assert(task.arr.slice(-1).pop().calendar_id=='2018-6-29');
			task.addFromPolicy(policy_obj, '2018-7-1');
			assert(task.arr.slice(-1).pop().calendar_id=='2018-7-1');
			task.addFromPolicy(policy_obj, '2018-7-1');
			assert(task.arr.slice(-1).pop().calendar_id=='2018-7-3');
			task.addFromPolicy(policy_obj, '2018-7-1');
			assert(task.arr.slice(-1).pop().calendar_id=='2018-7-3');
			task.addFromPolicy(policy_obj, '2018-7-1');
			assert(task.arr.slice(-1).pop().calendar_id=='2018-7-5');	
			done();
		});
	});
});






