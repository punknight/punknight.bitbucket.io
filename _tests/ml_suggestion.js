var sin_reg_class = require('../sin_reg.js');
var sin_reg = new sin_reg_class();
var getMLCoefficients = sin_reg.getMLCoefficients;
var printSinWeek = sin_reg.printSinWeek;

var gantt_arr = [
	{gantt_id: 'g1', goal_id: 'goal-C', calendar_id: '2018-6-30', numerator_hours: 2, denominator_hours: 3, status: 'IN_PROGRESS', color:'#66ADD5', journal_text: ''},
	{gantt_id: 'g2', goal_id: 'goal-C', calendar_id: '2018-6-31', numerator_hours: 2, denominator_hours: 3, status: 'IN_PROGRESS', color:'#66ADD5', journal_text: ''},
	{gantt_id: 'g3', goal_id: 'goal-C', calendar_id: '2018-7-1', numerator_hours: 2, denominator_hours: 3, status: 'IN_PROGRESS', color:'#66ADD5', journal_text: ''},
	{gantt_id: 'g4', goal_id: 'goal-C', calendar_id: '2018-7-4', numerator_hours: 2, denominator_hours: 3, status: 'IN_PROGRESS', color:'#66ADD5', journal_text: ''},
	{gantt_id: 'g5', goal_id: 'goal-C', calendar_id: '2018-7-5', numerator_hours: 2, denominator_hours: 3, status: 'IN_PROGRESS', color:'#66ADD5', journal_text: ''},
	{gantt_id: 'g6', goal_id: 'goal-C', calendar_id: '2018-7-6', numerator_hours: 2, denominator_hours: 3, status: 'IN_PROGRESS', color:'#66ADD5', journal_text: ''},
]

var gantt_by_id = [
	[0, 1, 2, -1, -1, 3, 4, 5]
];

var points_arr = gantt_by_id[0].map((gantt_index, point_index)=>{
	if(gantt_index!=-1){
		return (gantt_arr[gantt_index].status == 'IN_PROGRESS') ? {x: point_index, y: 1} : {x: point_index, y: 0};
	} else {
		return {x: point_index, y: 0}
	}
});
var output_obj = getMLCoefficients(points_arr);
var test_input = [7, 8, 9, 10, 11, 12, 13];
var test_week	 = printSinWeek(output_obj, test_input);
console.log("output_obj: ", output_obj);
console.log("test_week: ", test_week);
