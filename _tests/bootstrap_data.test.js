var assert = require('assert')
var Data = require('../bootstrap_data.js');
var date_calc = require('../date_calc');

var date_calc_obj = new date_calc();
var data = {};

describe('Data', function(){
	describe('constructor', function(){
		it('should create Data object', function(done){
			data = new Data();
			assert(data.hasOwnProperty('data'));
			done();
		});
	});
	
	describe('bootstrapData', function(){
		it('should bootstrap a policy_arr, gantt_arr, and task_arr', function(done){
			data.bootstrapData(date_calc_obj);
			assert(data.data.gantt_arr.length==6);
			assert(data.data.task_arr.length==6);
			assert(data.data.policy_arr.length==1);
			done();
		});
	});
});