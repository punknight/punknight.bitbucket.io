var assert = require('assert')
var Data = require('../bootstrap_json.js');
var dan_data = require('../dan_data.js');
var date_calc = require('../date_calc');

var date_calc_obj = new date_calc();
var day_num = new Date().getDay();
day_num = (day_num==0) ? 6 : day_num;
var date_calc_monday = date_calc_obj.date_adder([0, 0, 1-day_num]);	
var data = {};
describe('Data', function(){
	describe('constructor', function(){
		it('should create Data object', function(done){
			data = new Data(dan_data);
			assert(data.hasOwnProperty('data'));
			done();
		});
	});
	
	describe('bootstrapJSON', function(){
		it('should bootstrap a policy_arr, gantt_arr, and task_arr', function(done){
			data.bootstrapJSON(date_calc_monday);
			
			assert(data.policy_arr.length==3);
			
			assert(data.task_arr.length==56);
			
			assert(data.gantt_arr.length==20);
			done();
		});
	});
});