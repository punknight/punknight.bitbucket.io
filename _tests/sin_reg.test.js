var assert = require('assert');
var sin_reg_class = require('../sin_reg5.js');
var sin_reg = new sin_reg_class();
var getMLCoefficients = sin_reg.getMLCoefficients;
var printSinWeek = sin_reg.printSinWeek;

describe('sin_reg', function(){

	var points_arr = [
			{x: 1, y: 1},
			{x: 2, y: 1},
			{x: 3, y: 0},
			{x: 4, y: 0},
			{x: 5, y: 1},
			{x: 6, y: 1},
			{x: 7, y: 0},
			{x: 8, y: 0},
			{x: 9, y: 1},
			{x: 10, y: 1},
			{x: 11, y: 0},
			{x: 12, y: 0},
			{x: 13, y: 1},
			{x: 14, y: 1},
	];
		var points_arr2 = [
			{x: 1, y: 1},
			{x: 2, y: 1},
			{x: 3, y: 1},
			{x: 4, y: 1},
			{x: 5, y: 1},
			{x: 6, y: 1},
			{x: 7, y: 1},
	];
	var output_obj = {};
	describe('#getMLCoefficients', function(){
		it('should get some test_coefficients using machine learning', function(done){
			output_obj = getMLCoefficients(points_arr);
			assert(typeof output_obj.a0 != 'undefined');
			assert(typeof output_obj.a1 != 'undefined');
			assert(typeof output_obj.b0 != 'undefined');
			assert(typeof output_obj.b1 != 'undefined');
			console.log(output_obj);
			console.log('the period is: ', Math.PI*2/output_obj.a1);
			console.log('sin_reg1 (hardcracking) benchmark is currently at 24249ms with big_o of 35402500');
			console.log('sin_reg2 (delta_s optimization) benchmark is currently at 1013ms with big_o of 219223');
			done();
		});
	});
	
	describe('#printSinWeek', function(){
		it('should print a sine_wave of data similar to input', function(done){
			var test_input = points_arr.map((item)=>{return item.x});
			var test_output = points_arr.map((item)=>{return item.y});
			var actual_output = printSinWeek(output_obj, test_input);
			console.log('test_output: ', test_output);
			console.log('actual output:', actual_output);
			test_input.map((item, index)=>{
				assert(test_output[index]==actual_output[index])	
			});
			done();
		});
		it('should give default coefficients', function(){
			var ml_coefficients = {a0: 1, a1: 1, b0: 1, b1: 1};
			var test_input = points_arr.map((item)=>{return item.x});
			var actual_output = printSinWeek(ml_coefficients, test_input);
			console.log(actual_output);
		});
	});
});

