function saveProfile(filename, data) {
    var text = JSON.stringify(data);
    var pom = document.createElement('a');
    pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    pom.setAttribute('download', filename);

    if (document.createEvent) {
        var event = document.createEvent('MouseEvents');
        event.initEvent('click', true, true);
        pom.dispatchEvent(event);
    }
    else {
        pom.click();
    }
}
function loadProfile(){

  var pom = document.createElement('input');
  pom.setAttribute('type', 'file');
  pom.setAttribute('onchange', 'processFiles(this.files)');

  
  if (document.createEvent) {
        var event = document.createEvent('MouseEvents');
        event.initEvent('click', true, true);
        pom.dispatchEvent(event);
        
    }
    else {
        
        pom.click();

    }
    
}
function processFiles(files){
  var file = files[0];
  var reader = new FileReader();
  reader.onload = function(e){
    setData(e.target.result);
    location.reload();
  }
  reader.readAsText(file);
  //location.reload();
}
function clearData(){
  localStorage.setItem('data', JSON.stringify({policy_arr: [], task_arr: [], gantt_arr: []}));
  location.reload();
}
function setData(data){
  localStorage.setItem('data', JSON.stringify(data)); 
  
}
function getData(){
  return JSON.parse(localStorage.getItem('data'));
}